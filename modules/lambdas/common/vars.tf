variable "lambda_handler" {
  type        = string
  description = "Common handler for lambdas"
  default     = "src/index.handler"
}

variable "lambda_runtime" {
  type        = string
  description = "Common runtime for lambdas"
  default     = "nodejs16.x"
}