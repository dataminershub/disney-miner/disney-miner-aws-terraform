# SCHEDULER FOR PULLING DATA

resource "aws_cloudwatch_event_rule" "calendar_watch_rule_5_min" {
  name                = "CalendarWatch5min"
  description         = "Rule that triggers every 5 minutes"
  schedule_expression = "rate(5 minutes)"
}
