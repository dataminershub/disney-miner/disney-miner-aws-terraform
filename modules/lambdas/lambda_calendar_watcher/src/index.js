const AWS = require('aws-sdk');
const sns = new AWS.SNS();
const calendarScraper = require('./calendarScraper');

module.exports.handler = async (event, context) => {
  let body;
  let statusCode = 200;
  const headers = {
    "Content-Type": "application/json",
  };

  try {
    const attributes = await calendarScraper.checkCalendar()
    const targetKey = "disabled"

    if (attributes.hasOwnProperty(targetKey)) {
      const params = {
        Message: `Disney Date Available!: ${attributes["aria-label"]}, ${attributes.slot}`,
        TopicArn: process.env.sns_topic_arn,
      };
      try {
        // Publish the message to the SNS topic
        await sns.publish(params).promise();
    
        return {
          statusCode: 200,
          body: JSON.stringify('SMS sent successfully'),
        };
      } catch (error) {
        console.error('Error publishing SMS:', error);
        throw error;
      }
    } else {
      console.log(`Object does not have the key: ${targetKey}`);
    }
  } catch (err) {
    statusCode = 500;
    body = err.message;
  } finally {
    body = JSON.stringify(body);
  }

  return {
    statusCode,
    body,
    headers,
  };
};
