// Loading the dependencies. We don't need pretty
// because we shall not log html to the terminal
const axios = require("axios");
const chromium = require('chrome-aws-lambda');
const puppeteer = require('puppeteer-core');

const userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36';

async function getSiteData(reportUrl) {
  const {data} =  await axios.get(reportUrl, {
    headers: {
      'User-Agent': userAgent,
      // Add any other headers if needed
    },
  })
  return data;
}
// Async function which scrapes the data
async function scrapeData() {
  try {
    // Fetch HTML of the page we want to scrape
    const data = await getSiteData("https://disneyland.disney.go.com/passes/blockout-dates/")
    // Load HTML we fetched in the previous line
    const $ = cheerio.load(data);
    }
  catch (err) {
    console.error(err);
  }
}

async function checkCalendar() {
     // return await scrapeData()
     // Launch a headless browser
     let browser = null;
     try {
      browser = await puppeteer.launch({
        executablePath: await chromium.executablePath,
        args: chromium.args,
        headless: true, // Set to false for debugging
      });


  // Open a new page
  const page = await browser.newPage();
  await page.setUserAgent(userAgent);

  // Navigate to the webpage
  await page.goto('https://disneyland.disney.go.com/passes/blockout-dates/');
  await page.evaluate(() => {
    const middle = Math.ceil(document.body.scrollHeight / 2);
    window.scrollTo(0, middle);
  });

const getEnchantPass = await page.evaluate(() => {
  const elementInShadowDOM = document.querySelector("body > app-root > div > app-layout > div > div.pageContentContainer > div.pageContentWrapper.pageLoaded > div > app-blockout-dates-page > div > div.pageContentContainer > div > app-section-pass-selection > div > com-park-admission-calendar-pass-selection").shadowRoot.querySelector("#enchant-key-pass > div");
  // Check if the element is found
  if (elementInShadowDOM) {
    // Trigger a click on the element
    elementInShadowDOM.click();

    // Return any additional information if needed
    return { clicked: true, additionalInfo: "Element clicked successfully" };
  } else {
    // Return information indicating that the element was not found
    return { clicked: false, additionalInfo: "Element not found" };
  }
  })
  await page.waitForTimeout(2000);

  const dataAttributes = await page.evaluate(() => {
    const elementInShadowDOM = document.querySelector("body > app-root > div > app-layout > div > div.pageContentContainer > div.pageContentWrapper.pageLoaded > div > app-blockout-dates-page > div > div.pageContentContainer > div > div.calendarContainer > app-section-calendar-container > div > com-park-admission-calendar-container").shadowRoot.querySelector("#admissionCalendar").shadowRoot.querySelector("#calendar1 > div:nth-child(19)")
    // Check if the element is found
    if (elementInShadowDOM) {
      // Trigger a click on the element
      const attributes = {};
      for (const attr of elementInShadowDOM.attributes) {
        attributes[attr.name] = attr.value;
      }
  
      // Return any additional information if needed
      return attributes;
    } else {
      // Return information indicating that the element was not found
      return null;
    }
  })

  return dataAttributes;
} catch (error) {
  return error;
} finally {
  if (browser !== null) {
    await browser.close();
  }
}
}

module.exports = {
  checkCalendar
}