resource "aws_lambda_function" "lambda_calendar_watcher" {
  filename      = "./modules/archive/lambda_calendar_watcher.zip"
  function_name = "${local.project_prefix}-calendar-watcher"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = var.lambda_handler

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
  source_code_hash = filebase64sha256("./modules/archive/lambda_calendar_watcher.zip")

  layers  = [aws_lambda_layer_version.calendar_watcher_layer.arn]
  runtime = var.lambda_runtime
  timeout = 60
  tags    = local.tags

  environment {
    variables = {
      ACCESS_KEY    = data.aws_ssm_parameter.aws_access_key.value
      ACCESS_KEY_ID = data.aws_ssm_parameter.aws_access_id.value
      SNS_TOPIC_ARN = aws_sns_topic.disney_calendar_topic.arn,

    }
  }
}

# CLOUD WATCH LOGS
resource "aws_cloudwatch_log_group" "lambda_calendar_watcher_logs" {
  name              = "/aws/lambda/${aws_lambda_function.lambda_calendar_watcher.function_name}"
  retention_in_days = 1
}

# SCHEDULER FOR PULLING DATA
resource "aws_cloudwatch_event_target" "check_disney_calendar" {
  rule      = aws_cloudwatch_event_rule.calendar_watch_rule_5_min.name
  target_id = "lambda_calendar_watcher"
  arn       = aws_lambda_function.lambda_calendar_watcher.arn
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_lambda_calendar_watcher" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_calendar_watcher.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.calendar_watch_rule_5_min.arn
}
