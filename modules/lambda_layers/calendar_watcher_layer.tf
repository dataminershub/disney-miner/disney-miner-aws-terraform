resource "aws_lambda_layer_version" "calendar_watcher_layer" {
  filename   = "./modules/layer_lambda_calendar_watcher.zip"
  layer_name = "calendar_watcher_layer"

  compatible_runtimes = ["nodejs16.x"]
}
