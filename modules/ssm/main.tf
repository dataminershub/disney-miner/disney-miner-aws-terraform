# pull these parameters from admin terraform project
data "aws_ssm_parameter" "aws_access_key" {
  name = "access_key"
}

data "aws_ssm_parameter" "aws_access_id" {
  name = "access_key_id"
}