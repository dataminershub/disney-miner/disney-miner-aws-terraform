variable "tags_file" {
  default = "tags.json"
}

variable "project_name" {
  default = "dm-aws-tf"
}

locals {
  account_id = (data.aws_caller_identity.current.account_id)
  tags       = jsondecode(file(var.tags_file))

  project_prefix = "${var.project_name}-${lower(var.branch)}"

}

terraform {
  backend "s3" {
    bucket = "dm.bucket.tf.state.files"
    key    = "terraform_state_files/dm-aws-tf/main.tfstate" # Change the suffix when switching branches
    region = "us-west-1"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

provider "aws" {
  profile = ""
  region  = "us-west-1"
}
data "aws_caller_identity" "current" {}
data "aws_region" "current" {}

