resource "aws_sns_topic" "disney_calendar_topic" {
  name = "disney_calendar_topic"
}

resource "aws_sns_topic_subscription" "disney_calendar_topic_subscription" {
  topic_arn = aws_sns_topic.disney_calendar_topic.arn
  protocol  = "sms"
  endpoint  = "+16024764189" # Replace with your phone number
}